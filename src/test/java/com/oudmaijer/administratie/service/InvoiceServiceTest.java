package com.oudmaijer.administratie.service;

import com.oudmaijer.administratie.config.AppConfig;
import com.oudmaijer.administratie.config.DispatcherConfig;
import com.oudmaijer.administratie.core.domain.invoice.Invoice;
import com.oudmaijer.administratie.core.service.InvoiceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class}, loader=AnnotationConfigContextLoader.class)
public class InvoiceServiceTest {

    @Autowired
    InvoiceService invoiceService;

    @Test
    public void doSomeTest() {
        invoiceService.addInvoice(new Invoice());
    }
}
