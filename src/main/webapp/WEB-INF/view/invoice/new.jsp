<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

Nieuwe factuur

<form:form modelAttribute="invoice" action="add" method="put">
    <p><form:errors path="*"/></p>
    <p>Opmerking: <form:input path="comment" type="text" name="comment"/></p>
    <input type="submit"/>
</form:form>
