<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div>
    <p>Facturen (${success})</p>
    <table>
    <c:forEach items="${invoices}" var="item">
        <tr>
            <td><a href="/app/invoice/${item.id}">Details van factuur #${item.id}</a></td>
            <td>${item.comment}</td>
        </tr>
    </c:forEach>
    </table>
</div>