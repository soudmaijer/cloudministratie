<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
<head>
    <!-- jQuery -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

    <!-- jQuery ScrollTo Plugin -->
    <script defer src="http://balupton.github.com/jquery-scrollto/scripts/jquery.scrollto.min.js"></script>

    <!-- History.js -->
    <script defer src="http://balupton.github.com/history.js/scripts/bundled/html4+html5/jquery.history.js"></script>

    <!-- This Gist -->
    <script defer src="http://gist.github.com/raw/854622/ajaxify-html5.js"></script>
</head>
<body>
    <h2>Bladiebla</h2>
    <tiles:insertAttribute name="content" />
</body>
</html>