package com.oudmaijer.administratie.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;
import org.springframework.web.servlet.view.tiles2.TilesConfigurer;
import org.springframework.web.servlet.view.tiles2.TilesView;
import org.springframework.web.servlet.view.xml.MarshallingView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Configuration
@ComponentScan("com.oudmaijer.administratie.web")
@EnableWebMvc
public class DispatcherConfig extends WebMvcConfigurerAdapter {

    @Bean
    public TilesConfigurer tilesConfigurer() {
        TilesConfigurer tc = new TilesConfigurer();
        tc.setDefinitions(new String[] {"WEB-INF/tiles-definitions.xml"});
        return tc;
    }

    public UrlBasedViewResolver tilesViewResolver() {
        UrlBasedViewResolver tvr = new UrlBasedViewResolver();
        tvr.setViewClass(TilesView.class);
        return tvr;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("classpath:/view/static/");
    }

    @Bean
    public ContentNegotiatingViewResolver contentNegotiatingViewResolver() {
        ContentNegotiatingViewResolver contentNegotiatingViewResolver = new ContentNegotiatingViewResolver();

        Map<String, String> mediaTypes = new HashMap<String, String>();
        mediaTypes.put("html", MediaType.TEXT_HTML_VALUE);
        mediaTypes.put("json", MediaType.APPLICATION_JSON_VALUE);
        mediaTypes.put("xml", MediaType.APPLICATION_XML_VALUE);
        contentNegotiatingViewResolver.setMediaTypes(mediaTypes);

        List<ViewResolver> vr = new ArrayList<ViewResolver>();
        vr.add(tilesViewResolver());
        contentNegotiatingViewResolver.setViewResolvers(vr);

        List<View> views = new ArrayList<View>();
        views.add(new MappingJacksonJsonView());
        views.add(new MarshallingView());
        contentNegotiatingViewResolver.setFavorPathExtension(true);
        contentNegotiatingViewResolver.setDefaultViews(views);
        contentNegotiatingViewResolver.setDefaultContentType(MediaType.TEXT_HTML);

        return contentNegotiatingViewResolver;
    }
}
