package com.oudmaijer.administratie.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.ArrayList;
import java.util.Collection;

@Configuration
@EnableTransactionManagement
@ComponentScan("com.oudmaijer.administratie.core")
@EnableCaching
@ImportResource("classpath:/drools-context.xml")
public class AppConfig {

    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
        Collection<ConcurrentMapCache> caches = new ArrayList<ConcurrentMapCache>();
        ConcurrentMapCache bean = new ConcurrentMapCache("default");
        caches.add(bean);
        simpleCacheManager.setCaches(caches);
        return simpleCacheManager;
    }
}
