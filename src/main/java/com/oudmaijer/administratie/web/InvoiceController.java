package com.oudmaijer.administratie.web;

import com.oudmaijer.administratie.core.domain.invoice.Invoice;
import com.oudmaijer.administratie.core.service.InvoiceService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.inject.Inject;
import javax.validation.Valid;

@Controller
@RequestMapping("/invoice")
public class InvoiceController {

    private InvoiceService invoiceService;

    @Inject
    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String details(@PathVariable("id") Long id, Model model) {
        model.addAttribute( invoiceService.getInvoiceById(id) );
        return "invoice/details";
    }

    @RequestMapping(value = "/add", method = RequestMethod.PUT)
    public String addInvoice(@Valid Invoice invoice, BindingResult errors, RedirectAttributes redirectAttributes) {
        if(errors.hasErrors()) {
            return "invoice/new";
        }
        invoiceService.addInvoice(invoice);
        redirectAttributes.addFlashAttribute("success", "true");
        return "redirect:/app/invoice/list";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listInvoices(Model model) {
        model.addAttribute("invoices",  invoiceService.getInvoices());
        return "invoice/list";
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newInvoice(Model model) {
        model.addAttribute("invoice", new Invoice());
        return "invoice/new";
    }
}

