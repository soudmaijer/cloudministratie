package com.oudmaijer.administratie.core.domain.invoice

public enum InvoiceType {
    INCOMING, OUTGOING;
}
