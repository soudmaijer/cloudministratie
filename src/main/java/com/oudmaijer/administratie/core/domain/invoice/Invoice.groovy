package com.oudmaijer.administratie.core.domain.invoice

import com.oudmaijer.administratie.core.domain.payment.Payment
import com.oudmaijer.administratie.core.domain.tax.TaxItem
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class Invoice {
    Long id = 1;
    @NotNull
    @Size(min=5)
    String comment;
    Date invoiceDate;
    Payment payment;
    Double amount;
    InvoiceType type;
    InvoiceCategory category;
    List<InvoiceAttachment> attachments;
    List<TaxItem> taxItems;
}
