package com.oudmaijer.administratie.core.service;

import com.oudmaijer.administratie.core.domain.invoice.Invoice;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
@Named("invoiceService")
public class InvoiceService {

    // Temp dummy storage
    private static ConcurrentMap<Long, Invoice> invoices = new ConcurrentHashMap<Long, Invoice>();

    static {
        invoices.put(1L, new Invoice());
    }

    @Inject
    private InvoiceRuleService ruleService;

    @Cacheable(value = "default", condition = "#id > 0")
    public Invoice getInvoiceById(Long id) {
        return invoices.get(id);
    }

    public Invoice addInvoice(Invoice invoice) {
        invoice = ruleService.calculateVat(invoice);
        invoices.put(invoice.getId(), invoice);
        return invoice;
    }

    public Collection<Invoice> getInvoices() {
        return invoices.values();
    }
}
