package com.oudmaijer.administratie.core.service;

import com.oudmaijer.administratie.core.domain.invoice.Invoice;
import org.drools.runtime.StatefulKnowledgeSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Qualifier;

@Service
public class InvoiceRuleService {

    @Inject @Named("vat")
    private StatefulKnowledgeSession session;

    public Invoice calculateVat(Invoice invoice) {
        session.insert(invoice);
        session.fireAllRules();
        return invoice;
    }
}
